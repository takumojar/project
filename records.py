# Редактор базы record
import sys
import os
import sqlite3
import Console

Limit = 33


def main():
    options = dict(a=add_record, l=records_list, f=find, q=quit_from)
    filename = os.path.join(os.path.dirname(__file__), 'record.sdb')
    db = None
    try:
        db = connect(filename)
        action = ""
        while True:
            count = records_count(db)
            print('\nRecords ({})'.format(os.path.basename(filename)))
            if action != 1 and 1 <= count < Limit:
                records_list(db)
            print()
            menu = ("(A)dd (L)ist (F)ind (Q)uit"
                    if count else "(A)dd (Q)uit")
            valid = frozenset('alfq' if count else 'aq')
            action = Console.get_menu_choice(menu, valid)
            options[action](db)
    finally:
        if db is not None:
            db.close()


def connect(filename):
    create = not os.path.exists(filename)
    db = sqlite3.connect(filename)
    if create:
        cursor = db.cursor()
        cursor.execute("CREATE TABLE info("
                       "id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, "
                       "name TEXT NOT NULL, "
                       "address TEXT NOT NULL)")
        cursor.execute("CREATE TABLE accounts("
                       "id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, "
                       "username TEXT NOT NULL, "
                       "password TEXT NOT NULL,"
                       "comments TEXT)")
        cursor.execute("CREATE TABLE base(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, name TEXT NOT NULL,"
                       "math INTEGER, physics INTEGER, history INTEGER, biology INTEGER, chemistry INTEGER,"
                       "info_id INTEGER, accounts_id INTEGER, FOREIGN KEY (info_id) REFERENCES info,"
                       "FOREIGN KEY (accounts_id) REFERENCES accounts)")
        db.commit()
    return db


def add_record(db):
    import hashlib
    username = Console.get_string('Username', 'username')
    password = Console.get_string('Password', 'password')
    sha = hashlib.sha256()
    sha.update(password.encode('utf-8'))
    password = sha.hexdigest()
    name = Console.get_string('Name', 'name')
    address = Console.get_string('Address', 'address')
    math = Console.get_integer('Math', 'math')
    physics = Console.get_integer('Physics', 'physics')
    history = Console.get_integer('History', 'history')
    accounts_id = get_and_set_accounts(db, username, password, )
    info_id = get_and_set_info(db, name, address)
    cursor = db.cursor()
    cursor.execute("INSERT INTO base ('name', 'math', 'physics', 'history', 'accounts_id', 'info_id')"
                   "VALUES (?, ?, ?, ?, ?, ?)", (name, math, physics, history, accounts_id, info_id))
    db.commit()


def get_and_set_accounts(db, username, password):
    accounts_id = get_accounts_id(db, username, password)
    if accounts_id is not None:
        return accounts_id
    cursor = db.cursor()
    cursor.execute("INSERT INTO accounts ('username', 'password') VALUES (?, ?)", (username, password))
    db.commit()
    return get_accounts_id(db, username, password)


def get_accounts_id(db, username, password):
    cursor = db.cursor()
    cursor.execute("SELECT id FROM accounts WHERE username=? and password=?", (username, password))
    fields = cursor.fetchone()
    return fields[0] if fields is not None else None


def get_and_set_info(db, name, address):
    info_id = get_info_id(db, name, address)
    if info_id is not None:
        return info_id
    cursor = db.cursor()
    cursor.execute("INSERT INTO info ('name', 'address') VALUES (?, ?)", (name, address))
    db.commit()
    return get_info_id(db, name, address)


def get_info_id(db, name, address):
    cursor = db.cursor()
    cursor.execute("SELECT id FROM info WHERE name=? and address=?", (name, address))
    fields = cursor.fetchone()
    return fields[0] if fields is not None else None


def records_count(db):
    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM base")
    return cursor.fetchone()[0]


def records_list(db):
    cursor = db.cursor()
    sql = ("SELECT base.name, base.math, base.physics, base.history, info.name, info.address,"
           "accounts.username FROM base, info, accounts WHERE base.info_id=info.id and base.accounts_id = accounts.id")
    start = None
    if records_count(db) > Limit:
        start = Console.get_string("List those starting with [Enter=all]", "start")
        sql += " And base.name LIKE ?"
    sql += " ORDER BY base.name"
    print()
    if start is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, (start + "%",))
    for record in cursor:
        print('{0[0]} {0[1]} {0[2]} {0[3]}'.format(record))


def find(db):
    name, identity = find_records_template(db, 'find')
    cursor = db.cursor()
    cursor.execute("SELECT base.name, base.math, base.physics, base.history FROM base WHERE id=?", (identity,))
    for record in cursor:
        print('У {0[0]}: {0[1]}, {0[2]}, {0[3]}'.format(record))


def find_records_template(db, message):
    message = "Enter name " + message
    cursor = db.cursor()
    while True:
        start = Console.get_string(message, 'name')
        if not start:
            return None, None
        cursor.execute("SELECT username, id FROM accounts WHERE username LIKE ? ORDER BY username", (start + "%",))
        records = cursor.fetchall()
        if len(records) == 0:
            print("No results by your query", start)
            continue
        elif len(records) == 1:
            return records[0]
        elif len(records) > Limit:
            print('Too many results, it cannot be')


def quit_from(db):
    if db is not None:
        db.commit()
        db.close()
        print('Saved!')
    sys.exit()


main()
