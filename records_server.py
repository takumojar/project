import argparse
import datetime
import sys
import xmlrpc.server
import Manage
if sys.version_info[:2] > (3, 1):
    import warnings
    warnings.simplefilter("ignore", ResourceWarning)


HOST = "localhost"
PORT = 11002
PATH = "/records"


class RequestHandler(xmlrpc.server.SimpleXMLRPCRequestHandler):
    rpc_paths = (PATH,)


def main():
    """Функция получает имя хоста и номер порта,
     создает объекты Manage.Manager и xmlrpc.server.SimpleXMLRPCServer,
     после чего начинает обслуживать запросы
    """
    host, port, notify = handle_commandline()
    manager, server = setup(host, port)
    print("Server startup at {} on {}:{}{}".format(
            datetime.datetime.now().isoformat()[:19], host, port, PATH))
    try:
        if notify:
            with open(notify, "wb") as file:
                file.write(b"\n")
        server.serve_forever()
    except KeyboardInterrupt:
        print("\rServer shutdown at {}".format(
                datetime.datetime.now().isoformat()[:19]))
        manager._dump()


def handle_commandline():
    parser = argparse.ArgumentParser(conflict_handler="resolve")
    parser.add_argument("-h", "--host", default=HOST,
            help="hostname [default %(default)s]")
    parser.add_argument("-p", "--port", default=PORT, type=int,
            help="port number [default %(default)d]")
    parser.add_argument("--notify", help="specify a notification file")
    args = parser.parse_args()
    return args.host, args.port, args.notify


def setup(host, port):
    """Создание менеджера данных и сервера """
    manager = Manage.Manager()
    server = xmlrpc.server.SimpleXMLRPCServer((host, port),
            requestHandler=RequestHandler, logRequests=False)
    server.register_introspection_functions()
    for method in (manager.login, manager.get_info, manager.submit_comments, manager.get_status):
        server.register_function(method)
    return manager, server


if __name__ == "__main__":
    main()
