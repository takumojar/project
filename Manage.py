# Менеджер для связи сервер/клиент
import collections
import hashlib
import datetime
import sys
import xmlrpc.client
import sqlite3
import random

random.seed(917)

Comments = collections.namedtuple("Comments", "when comments username")


class Error(Exception):
    pass


def generate_dicts(cur):
    """Создание словаря из базы """
    fields = [d[0].lower() for d in cur.description]
    while True:
        rows = cur.fetchmany()
        if not rows:
            return
        for row in rows:
            yield dict(zip(fields, row))


class Manager:

    SessionId = 0
    UsernameSessionID = {}
    ReadingForCount = {}

    def login(self, username, password):
        name = name_for_identity(username, password)
        if name is None:
            raise Error("Invalid username or password")
        Manager.SessionId += 1
        sessionid = Manager.SessionId
        Manager.UsernameSessionID[sessionid] = username
        return sessionid, name

    def get_status(self, sessionid):
        """Метод вычисляет и возвращает сколько раз пользователь изменял свои данные и
         общую сумму, кототрую обработал сервер с момента запуска
         """
        username = self._username_for_sessiondid(sessionid)
        count = total = 0
        for comments in Manager.ReadingForCount.values():
            if comments is not None:
                total += 1
                if comments.username == username:
                    count += 1
        return count, total

    def get_info(self, sessionid):
        """Метод берет данные из базы record.sdb и преобразует в заметку о пользователе"""
        self._username_for_sessiondid(sessionid)
        con = sqlite3.connect('record.sdb')
        cur = con.cursor()
        cur.execute("SELECT base.name, base.math, base.physics, base.history, accounts.username FROM base, accounts"
                    " WHERE base.accounts_id = accounts.id")
        for r in generate_dicts(cur):
            if r['username'] == self._username_for_sessiondid(sessionid):
                note = "У Вас, {0}: \nМатематика {1} \nФизика {2} \nИстоирия {3}" \
                        "\nОставтьте комментарий либо нажмите ентер," \
                        "чтобы выйти. ".format(r['name'], r['math'], r['physics'], r['history'])
                return note

    def _username_for_sessiondid(self, sessionid):
        """Метод либо возвращает имя пользователя из сеанса с указанием идентификатора,
        либо преобразует исключеие KeyError
        """
        try:
            return Manager.UsernameSessionID[sessionid]
        except KeyError:
            raise Error("Invalid session ID")

    def submit_comments(self, sessiondid, note, when, comments):
        """Метод принимает идентификатор сеанса, заметку о пользователе, комментарии """
        if isinstance(when, xmlrpc.client.DateTime):
            when = datetime.datetime.strptime(when.value, "%Y%m%dT%H:%M:%S")
        username = self._username_for_sessiondid(sessiondid)
        comments = Comments(when, comments, username)
        Manager.ReadingForCount[note] = comments
        con = sqlite3.connect("record.sdb")
        cur = con.cursor()
        cur.execute("UPDATE accounts SET comments=? WHERE accounts.username=?", (comments.comments, username))
        con.commit()
        return True

    @staticmethod
    def _dump(file=sys.stdout):
        """Отладка для проверки сохранение отправленных данных """
        for note, comments in sorted(Manager.ReadingForCount.items()):
            if comments is not None:
                print("{}={} [{}] at {}".format(note, comments.comments, comments.username, comments.when), file=file)


_User = collections.namedtuple("User", "username sha256")


def name_for_identity(username, password):
    """Функция вычисляет свертку указанного пароля по алгоритму SHA256,
     если комбинация имени пользователя с этой сверткой совпадает со словарем _USERS,
     возвращает истинное имя """
    sha = hashlib.sha256()
    sha.update(password.encode("utf-8"))
    user = _User(username, sha.hexdigest())
    return _Users.get(user)

con = sqlite3.connect('record.sdb')
cur = con.cursor()
cur.execute("SELECT base.name, accounts.username, accounts.password FROM base, accounts "
            "WHERE base.accounts_id = accounts.id")
_Users = {} # _Users = {'Jagar': 'a', 'kamina': 'lagann', '1': 1, 'new': 'new'}
for r in generate_dicts(cur):
    v = _User(r['username'], r['password'])
    _Users[v] = r['name']


if __name__ == "__main__":
    print("Loaded OK")
