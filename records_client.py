import argparse
import datetime
import getpass
import socket
import sys
import xmlrpc.client
import warnings
if sys.version_info[:2] > (3, 1):
    warnings.simplefilter("ignore", ResourceWarning)
if sys.version_info[:2] < (3, 3):
    ConnectionError = socket.error


HOST = "localhost"
PORT = 11002
PATH = "/records"


def main():
    """Функция получает имя хоста и номер порта сервера,
    а затем логин и пароль пользователя, после создает объект-заместитель экземпляра Manage.Manager
    """
    host, port = handle_commandline()
    username, password = login()
    if username is not None:
        try:
            manager = xmlrpc.client.ServerProxy("http://{}:{}{}".format(
                    host, port, PATH))
            sessionid, name = manager.login(username, password)
            print("Welcome, {}!".format(name))
            communication(manager, sessionid)
        except xmlrpc.client.Fault as err:
            print(err)
        except ConnectionError as err:
            print("Error: Is the server running? {}".format(err))


def handle_commandline():
    parser = argparse.ArgumentParser(conflict_handler="resolve")
    parser.add_argument("-h", "--host", default=HOST,
            help="hostname [default %(default)s]")
    parser.add_argument("-p", "--port", default=PORT, type=int,
            help="port number [default %(default)d]")
    args = parser.parse_args()
    return args.host, args.port


def login():
    loginName = getpass.getuser()
    username = input('Username [{}]: '.format(loginName))
    if not username:
        username = loginName
    password = getpass.getpass()
    if not password:
        return None, None
    return username, password


def communication(manager, sessionid):
    """Функция вызывается после успешной аутентификации пользователя для организации
    взаимодействий между сервером и клиентом
    """
    while True:
        note = manager.get_info(sessionid)
        accepted, comments = get_comments(note)
        if comments or not comments:
            submit(manager, sessionid, note, comments)
            break


def get_comments(note):
    """Функция обрабатывающая комментарий пользователя """
    comments = input('{}'.format(note))
    if comments:
        try:
            return True, str(comments)
        except ValueError:
            print("Invalid input")
            return False, 0
    else:
        return True, 0


def submit(manager, sessionid, note, comments):
    try:
        now = datetime.datetime.now()
        manager.submit_comments(sessionid, note, now, comments)
        count, total = manager.get_status(sessionid)
        print("Accepted your comment {} out of {}".format(count, total))
        return True
    except (xmlrpc.client.Fault, ConnectionError) as err:
        print(err)
        return False


if __name__ == '__main__':
    main()
